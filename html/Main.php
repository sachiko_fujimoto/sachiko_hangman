<?php
$setWord = $_POST['setWord'];//正解単語
$answerWord = $_POST['answerWord'];//回答アルファベット
$answerWords = array();//回答アルファベット配列
$hp = 6;//HP

//正解単語が入力されているか
if ($setWord) {
  //正解単語をカウント・伏字
  if ($setWord) {
    $count = mb_strlen($setWord);
    $hiddenWord=str_repeat(' _ ',mb_strlen($setWord));
  }


//正解だった場合、伏字中の正解アルファベットのみ表示させるテスト
// $string = '012345678';
// var_dump( $string );
// $a = '2';
// var_dump( strpos($string,$a) );
// /* 文字列の置換 */
// var_dump( substr_replace($string, '#',strpos($string,$a)) );//理想：01#345678→実際：01#
// var_dump( substr_replace($string, '#', 4, 3) );


  //回答アルファベットが入力されているか
  if ($answerWord != null) {
    $answerWords = $answerWord;
    //回答アルファベットが正解単語の中に含まれているか
    if (strpos($setWord,$answerWord) === false) {
      $hp = $hp-1;
      //HPが0ではないか
      if ($hp == 0) {
        echo "ゲームオーバー";
      }
    } else {
      $hiddenWord = str_replace($setWord,' _ ',$hiddenWord);//伏字とアルファベットを置換する予定だったが、機能していない

      //伏字に伏字が含まれているか
      if (strpos($hiddenWord,' _ ') === false)  {
        echo "クリア";
        echo "残りHPは".$hp."でした。";
        exit;
      }
    }

  }

}
 ?>

 <!DOCTYPE html>
 <html lang="ja">
   <head>
     <meta charset="utf-8">
     <title>もっちーのハングマン</title>
   </head>
   <body>
     <p>あなたのHP：<?php echo $hp;?></p>
     <p>セットされた単語は<?php echo $count;?>文字です。</p>
     <p>「<?php echo $hiddenWord;?>」</p>
     <br>
     <p>アルファベット1文字を入力してください。</p>
     <form action="Main.php" method="post">
       <input type="text" name="answerWord" size="10">
       <input type="hidden" name="setWord" value="<?php echo $_POST['setWord']; ?>">
       <input type="hidden" name="flag" value="f" method='post'>
       <input type="submit" value="登録">
     </form>
   </body>
 </html>
